import ExpiviIFrameService from './src/expivi-iframe.service';

const eventViewer = document.getElementById('event-viewer') as HTMLElement;

const win = window as any;

win.expiviService = new ExpiviIFrameService(win.frame as Window);

win.expiviService.setEventListener('*', event => {
    eventViewer.innerText += `${JSON.stringify(event)}\n`;
});

win.expiviService.setEventListener('v1.add-to-cart', event => {
    console.log('[Expivi][iFrame][Client]', event);

    return 'add-to-cart';
});

win.expiviService.setEventListener('v1.save-current-config', event => {
    console.log('[Expivi][iFrame][Client]', event);

    return { foo: 'bar' };
});

win.expiviService.setEventListener('v1.before-image-upload', event => {
    console.log('[Expivi][iFrame][Client]', event);

    return false;
});

win.expiviService.setEventListener('v1.price-changed', event => {
    console.log('[Expivi][iFrame][Client]', event);

    return false;
});

void (async function handleExpivi() {
    await win.expiviService.isReady();
    console.log('[Expivi][iFrame][Client] Ready!');
})();

win.sendEvent = (type, payload, asFinished) => {
    win.expiviService.sendMessage(
        type,
        payload,
        asFinished ? '00000000-0000-0000-0000-000000000000' : undefined,
    );
};
